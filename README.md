Log Extension for Yii 2
=========================

YII2的日志扩展，用于把各种日志统一输出，以方便接入阿里日志服务；
同时扩展了SQL日志、请求日志、配置日志，并支持各种日志配置；

安装&使用
------------
```
1 安装最新发布版本 composer require mingyuanyun/yii2-ilog
2 安装指定发布版本 composer require mingyuanyun/yii2-ilog:版本号
```
更新版本
修改composer.json文件，调整版本依赖如："mingyuanyun/yii2-ilog": "~1.0"表示允许大于等于1.0小于2.0的发布版本,
再执行
```
composer update mingyuanyun/yii2-ilog
```
2 若应用于web，修改web配置
```
2.1 bootstart增加iLog，如：'bootstrap' => ['log', 'iLog'],
2.2 modules中增加iLog基础配置
    'modules' => [
        'iLog' => [
            'class' => 'iLog\Module', //必须
            'levels' => ['request','database','error','info','warning'],
            // 可选,默认全部，配置要输出的日志级别，全集['config','request','database','error','info','trace','warning','profile']
            'categories' => ['info' => ['application']], //可选，默认全部，配置要过滤的日志类别
            //如：info日志只收集Application类别，则配置为'categories' => ['info' => ['application']] 注意大小写敏感
            'isFileSeparated' => false, // 可选，默认为false，不同级别的日志是否需要区分文件存储，如果设为true则在原ilog目录下生成ilog_request、ilog_error.log...等不同级别的日志文件
            'logFile' => '@runtime/ilog.log', //可选，配置输出文件路径，默认为runtime/ilog.log
            'maxFileSize' => 10240, //可选，最大文件大小，超出后文件进行翻滚即ilog.log.1 单位KB，默认10M
            'maxLogFiles' => 10, //可选，翻滚的最大文件数，默认10,
            'logIdFlag' => 'x-logid', //可选，请求header头中的日志ID标识，用于日志穿透,
            'subLogIdFlag' => 'x-sublogid', // 可选，请求header头中的调用方日志标识，用于应用间的层级调用
        ]
    ]
2.3 iLog其他配置（按需添加）
    'iLog' => [
        ...基础配置...
        'maskFields' => ['password'], // 可选，记录web请求日志时需要屏蔽的post参数，例如登陆请求中的密码等
        'customRequestParams' => ['o', 'x-tenant-code', 'x-uid', 'x-client-version', 'x-deviceid', 'x-logid'] // 可选，request日志中需要添加的自定义参数（取值顺序：1、get参数 2、header 3、cookie）
        'customFieldProviderClass' => '\iLog\CustomFieldProvider', // 可选，定义扩展字段读取类，该类继承iLog\base\CustomFieldProviderBase，重写相应方法会将字段追加到相应的日志类型中，请参考demo下的CustomFieldProvider
        'traceDesensitize' => [ // 可选，记录异常类日志时需加密的栈堆信息
            'key' => '', // 可选，加密key
            'policy' => [ // 加密策略，需加密的类方法，参考以下用法：
                "$class" => ["$method" => [$argIndex, ...]], // 指定方法，指定下标的参数加密。下标由0开始计算
                "$class" => ["$method"], // 指定方法，所有参数加密
            ],
            // 已内置对 PDO 账号密码加密的策略
            // 解密地址：https://web-utility.mysre.cn/ilog/decrypt?key=$key&encrypted=$encrypted
            // (key不填则使用默认值)
        ],
    ]
```
若需要前端做日志穿透请修改前端请求代码如:
$.ajax({
    headers: {
        x-logid: "c175bbcf2bf9"
    },
    type: "get",
    success: function (data) {
    }
});

若需要记录应用层级间的调用关系请修改调用端的代码如:
```
...
$curl = curl_init();
// 获取logid和subLogId
$iLog = \Yii::$app->getModule('iLog');
$header = [
    'x-logid' => $iLog->logId,
    'x-sublogid' => $iLog->subLogId
];
curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
curl_setopt($curl, CURLOPT_URL, 'http://XXXX.com');
...

```

3 若应用于console，修改console配置
```
3.1 bootstart增加iLog，如：'bootstrap' => ['log', 'iLog'],
3.2 components中增加iLog基础配置
    'components' => [
        'iLog' => [
            'class' => 'iLog\Component', //必须
            'levels' => ['request','database','error','info','warning'],
            //可选,默认全部，配置要输出的日志级别，全集['config','request','database','error','info','trace','warning','profile']
            'categories' => ['info' => ['application']], //可选，默认全部，配置要过滤的日志类别
            //如：info日志只收集Application类别，则配置为'categories' => ['info' => ['application']] 注意大小写敏感
            'isFileSeparated' => false, // 可选，默认为false，不同级别的日志是否需要区分文件存储，如果设为true则在原ilog目录下生成ilog_request、ilog_error.log...等不同级别的日志文件
            'logFile' => '@runtime/ilog.log', //可选，配置输出文件路径，默认为runtime/ilog.log
            'maxFileSize' => 10240, //可选，最大文件大小，超出后文件进行翻滚即ilog.log.1 单位KB，默认10M
            'maxLogFiles' => 10, //可选，翻滚的最大文件数，默认10,
            'logIdFlag' => 'XLogId', //可选，命令行参数--XLogId日志ID标识，用于日志穿透,
            'subLogIdFlag' => 'XSubLogId', // 可选，命令行参数--XSubLogId调用方日志标识，用于应用间的层级调用
        ]
    ]
3.3 iLog其他配置（按需添加）
    'iLog' => [
        ...基础配置...
        'customFieldProviderClass' => '\iLog\CustomFieldProvider', // 可选，定义扩展字段读取类，该类继承iLog\base\CustomFieldProviderBase，重写相应方法会将字段追加到相应的日志类型中，请参考demo下的CustomFieldProvider
        'traceDesensitize' => [ // 可选，记录异常类日志时需加密的栈堆信息
            'key' => '', // 可选，加密key
            'policy' => [ // 加密策略，需加密的类方法，参考以下用法：
                "$class" => ["$method" => [$argIndex, ...]], // 指定方法，指定下标的参数加密。下标由0开始计算
                "$class" => ["$method"], // 指定方法，所有参数加密
            ],
            // 已内置对 PDO 账号密码加密的策略
            // 解密地址：https://web-utility.mysre.cn/ilog/decrypt?key=$key&encrypted=$encrypted
            // (key不填则使用默认值)
        ],
    ]
```
**注意:**

**1、若要使web的logId穿透到命令行日志,需要修改你的命令行程序,若不需要穿透,不用做下述修改
其controller基类,增加如下,其它不需要修改:**
```
    public $XLogId;

    public function options($actionID)
    {
        return array_merge(parent::options($actionID), ['XLogId']);
    }
```
如：yii test/log p1 p2 --XLogId=c175bbcf2bf9
提示:若日志时间输出为UTC,请增加'timeZone' => 'Asia/Shanghai'到配置文件中;

**2、若要记录应用间的层级调用关系到命令行日志,需要修改你的命令行程序,若不需要记录层级关系,不用做下述修改
其controller基类,增加如下,其它不需要修改:**
```
    public $XSubLogId;

    public function options($actionID)
    {
        return array_merge(parent::options($actionID), ['XSubLogId']);
    }
```

```
// 获取XSubLogId
$iLog = \Yii::$app->getComponents(false)['iLog'];
$subLogId = $iLog->subLogId;
```

如：yii test/log p1 p2 --XSubLogId={$subLogId}

**3、由于准确获取sql执行的数据库连接信息需要改造yii源生的两个类加入获取代码，所以如果对数据库连接信息有准确要求的组请自行替换yii文件夹下面的两个文件，注意由于yii2小版本之间可能存在差异，请先对比这两个文件或者根据对比自行修改源码:
Command.php和Connection.php分别替换掉vendor/yiisoft/yii2/db下面的Command.php和Connection.php**

```
日志查询
------------
请联系运维开通子帐号，授权日志查询权限


性能影响
------------
经本机压测影响在毫秒级，更多性能测试结果可直接联系本人

[日志使用帮助] (https://confluence.mysre.cn/x/YIIW)
```




