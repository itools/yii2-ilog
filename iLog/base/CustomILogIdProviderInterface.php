<?php

namespace iLog\base;

interface CustomILogIdProviderInterface
{
    public function getCustomILogId();
}
