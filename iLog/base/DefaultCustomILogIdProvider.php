<?php

namespace iLog\base;

use iLog\helpers\StringHelper;

/**
 * Class CustomFieldBase 读取日志自定义参数的基类
 * @package iLog\base
 */
class DefaultCustomILogIdProvider implements CustomILogIdProviderInterface
{
    public function getCustomILogId()
    {
        if (extension_loaded("FastTracker") && class_exists("FastTracker")) {
            try {
                $traceId = \FastTracker::getTraceId();
                if (!preg_match('/^[a-zA-Z0-9_-]+$/', $traceId)) {
                    return StringHelper::logId();
                }
                return $traceId;
            } catch (\Exception $ex) {
                return null;
            }
        }
    }
}
