<?php

namespace iLog\helpers;

/**
 * Created by PhpStorm.
 * User: zhangqing
 * Date: 2020-10-19
 * Time: 11:16
 */
class StringHelper
{
    /**
     * 生成logId
     * @return string
     */
    public static function logId()
    {
        $uniqid = 'ilog_' . uniqid('', true);
        return str_replace('.', '', $uniqid);
    }
}
