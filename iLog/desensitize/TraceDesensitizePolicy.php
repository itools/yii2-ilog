<?php

namespace iLog\desensitize;

/**
 * @author yangyz <yangyz@mingyuanyun.com>
 * @since 1.4.7
 */
class TraceDesensitizePolicy extends \yii\base\Component
{
    /**
     * @var string
     */
    public $targetClass = '';

    /**
     * @var array custom index of arguments which should be insensitive. For example,
     *
     * ```php
     * [
     *     'someFunc' => [0, 2, 4] // specific function name and index of arguments
     *     'otherFunc', // each arguments of this function would be insensitive !
     * ]
     * ```
     *
     * an empty $targetFunc means no method would be insensitive
     */
    public $targetFunc = [];

    /**
     * @var TraceDesensitize
     */
    public $traceDesensitize = null;

    public function init()
    {
        if (empty($this->targetFunc)) {
            $this->targetFunc = [];
        } else {
            if (is_string($this->targetFunc)) {
                $this->targetFunc = [$this->targetFunc];
            }
        }
    }

    /**
     * @param array $trace one of debug_backtrace() returns value
     * @see https://www.php.net/manual/en/function.debug-backtrace.php
     *
     * @return string a stack trace string that had been desensize
     */
    public function getDesensizeTraceString($trace, $traceIndex)
    {
        $args = $trace['args'];
        $desensitizeArgs = [];

        $targetArgIndex = $this->getArgIndex($trace);
        foreach ($args as $index => $arg) {
            if (is_string($arg) || is_int($arg)) {
                $desensitizeArg = in_array($index, $targetArgIndex) ? $this->desensitizeArg($arg) : $arg;
                $desensitizeArgs[] = sprintf('\'%s\'', $desensitizeArg);
            } elseif (is_null($arg)) {
                $desensitizeArgs[] = 'NULL';
            } else {
                $desensitizeArgs[] = (string) $arg;
            }
        }

        return vsprintf('#%d %s(%d): %s%s%s(%s)', [
            $traceIndex,
            $trace['file'],
            $trace['line'],
            $trace['class'],
            $trace['type'],
            $trace['function'],
            implode(', ', $desensitizeArgs)
        ]);
    }

    /**
     * @param mixed $raw
     *
     * @return string
     */
    private function desensitizeArg($raw)
    {
        return $this->traceDesensitize->encrypt($raw);
    }

    /**
     * @param array $trace one of debug_backtrace() returns value
     *
     * @return array
     */
    private function getArgIndex($trace)
    {
        $targetArgIndex = [];

        if (!empty($this->targetFunc)) {
            foreach ($this->targetFunc as $funcName => $config) {
                if ($funcName === $trace['function']) {
                    $targetArgIndex = $config;
                } else if ($config === $trace['function']) {
                    // index is not set
                    $targetArgIndex = array_keys($trace['args']);
                }
            }
        }

        return array_unique($targetArgIndex);
    }
}
