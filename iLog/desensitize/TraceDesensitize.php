<?php

namespace iLog\desensitize;

/**
 * @author yangyz <yangyz@mingyuanyun.com>
 * @since 1.4.7
 */
class TraceDesensitize extends \yii\base\Component
{
    const DEFAULT_KEY = 'IOZRO70KSCAFTO3O';

    const DEFAULT_POLICY = [
        'PDO' => ['__construct' => [1,2]],
    ];

    /**
     * @var array
     */
    public $policy = [];

    /**
     * @var TraceDesensitizePolicy[]
     */
    private $_policy = [];

    /**
     * @var string the key use for encrypt
     */
    public $key = '';

    private $policyClass = 'iLog\desensitize\TraceDesensitizePolicy';

    /**
     * according to performance optimization, skip policy when one of below occurs:
     * 1. class is not found
     * 2. no method set
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $mergePolicy = \yii\helpers\ArrayHelper::merge(
            self::DEFAULT_POLICY,
            $this->policy
        );

        foreach ($mergePolicy as $targetClass => $targetFunc) {
            if (!class_exists($targetClass) || empty($targetFunc)) {
                continue;
            }

            $config = [
                'class' => $this->policyClass,
                'targetClass' => $targetClass,
                'targetFunc' => $targetFunc,
                'traceDesensitize' => $this,
            ];
            $this->_policy[$targetClass] = \yii::createObject($config);
        }

        if (empty($this->key)) {
            $this->key = self::DEFAULT_KEY;
        }
    }

    /**
     * @param string $raw
     *
     * @return string the encrypt string
     */
    public function encrypt($raw)
    {
        $res = openssl_encrypt($raw, 'DES-ECB', $this->key, 0, '');
        return base64_encode($res);
    }

    /**
     * @param mixed $id
     *
     * @return TraceDesensitizePolicy
     */
    public function getPolicy($id)
    {
        return $this->_policy[$id];
    }

    /**
     * @param \Throwable $t
     *
     * @return string return a custom trace string
     * @see https://www.php.net/manual/en/throwable.tostring
     */
    public function serialize(\Throwable $t)
    {
        $raw = (string) $t;
        $stringPieces = explode(PHP_EOL, $raw);
        // stack trace start from index 2
        $start = 2;
        $length = count($stringPieces);

        // access the original exception
        $origin = $t;
        $previous = $t->getPrevious();
        while (!empty($previous)) {
            $origin = $previous;
            $previous = $origin->getPrevious();
        }
        $traces = $origin->getTrace();

        for ($i = $start; $i < $length; $i++) {
            $index = $i - $start;
            $trace = $traces[$index];
            $targetClass = $trace['class'];
            $handler = $this->getPolicy($targetClass);

            if (!empty($handler)) {
                $stringPieces[$i] = $handler->getDesensizeTraceString($trace, $index);
            }
        }

        return implode(PHP_EOL, $stringPieces);
    }
}
